# Commission Payment Batch (Kata)

## Description
It is end of month and the sales of all goods and services of our company and which are managed in our brand new CRM System must 
be transferred to the *Commission Payment System* in order the involved salesperson gets his commission. 
The sales data which have to be sent to the *Commission Payment System* have the following attributes:

    Sale(salesId, rating, amount, customerId)

The `rating` describes what impact the sale has to our company, e.g. the sale can be *strategic*, *recurrent* or *temporary*. 
We want to achieve, besides the common sending to the *Commission Payment System*, that for *strategic* sales the CEO gets 
an email for a kind request to congratulate the salesperson personally. On the other hand when the `rating` is *recurrent*, 
this recurrent sale should be published on a webpage in our company's intranet which has a presumed REST API to make this done. 
In the near future further ratings will be introduced and handled individually as well.

An analogous batch run could have the following output:

    [04711, 'temporary', '299.98', 014]
    >>> Sending sale 04711 to the Commission Payment System
    [09345, 'strategic', '5,011,111', 755]
    >>> Sending sale 09345 to the Commission Payment System
    >>> Sending email to CEO for sale 09345
    [08922, 'recurrent', '745.99', 078]
    >>> Sending sale 08922 to the Commission Payment System
    >>> Publish sale 08922 to the intranet page
    ...


## Objectives
Develop the batch process that sends some sales data to the *Commission Payment System* but handles the different cases 
according to what is mentioned above. All the requests to the different systems can be doubled by stubs - that's not the focal point here. 
The attention in developing this should be turned to the design of the classes and the recognition of patterns. 
It exists not *the only one solution* but different eligible approaches.

Enjoy it!

